const SLASH = '/';
const TYPE = Symbol("type");
const DO_NOTHING = x => x;
const THROW = x => {throw x};

export const o = (input, output = {...(input || {})}) => output;
export const enumm = (...keys) => extend(keys, object(keys.map(key => [key, Symbol(key)])));
export const names = (...names) => extend(names, object(names.map(name => [name, name])));
export const keys = obj => Object.keys(obj);
export const entries = obj => Object.entries(obj);
export const extend = (obj, ...others) => Object.assign(obj, ...others);
export const concat = (...objs) => extend({}, ...objs);
export const apply = (obj, values) => entries(values || {}).forEach(([key, val]) => obj[key] = val) || obj;

// Some tings need a prototype.... we should have a create and prodcedure thing yhere maybe....
export const object = map => map.reduce((obj, [key, value]) => {
	obj[key] = value;
	return obj;
}, {});

export const normalise = path => path.endsWith(SLASH) ? path.substr(0, path.length - 1) : path;
export const buff = buffer => buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.byteLength);

export const iterator = (obj, iterator) => object(iterator(entries(obj)) || []);
export const iterate = (obj, iter = DO_NOTHING) => iterator(obj, entries => entries.forEach(iter));
export const map = (obj, mapper = DO_NOTHING) => iterator(obj, entries => entries.map(mapper));
export const every = (obj, condition = x => true) => iterator(obj, entries => entries.every(condition));
export const filter = (obj, filter = x => true) => iterator(obj, entries => entries.filter(filter));

export const copy_object = obj => map(obj, ([key, val]) => [key, copy(val)]);
export const copy_array = array => array.map(copy);
export const copy = item => type.literal(item) ? item : type.array(item) ? cp_array(item) : cp_object(item);

export const list = (...args) => new List(...args);
export class List extends Array {
	constructor(...args) {
		super(...args);
	}

	add(...items) {
		return items.forEach(x => this.push(x)) || this;
	}

	fill(filler = i => undefined) {
		for (let i = 0; i < this.length; i++)
			this[i] = filler(i);
		return this;
	}
}

export const funray = (...functions) => extend(
	(...args) => functions.map(x => x(...args)),
	{
		...functions,
		size: functions.length
	}
);

export const is = {
	...object([
		"function",
		"string",
		"number",
		"object",
		"symbol",
		"bigint",
		"undefined",
	].map(key => [key, val => typeof val === key])),

	int: x => type.number(x) && Number.isInteger(x),
	float: x => type.number(x) && !type.integer(x),

	type: (t, T) => t.constructor === T,
	// from: (t, T) => satisfies(t[TYPE], T),
	array: val => type.is(val, Array),
	date: val => type.is(val, Date),
};

// export const Descriptor = Type(
// 	META_TYPES.descriptor,
// 	construct.object,
// 	{
// 		constructor(handler) {
// 			this.extend({
// 				private: false,
// 				required: false,
// 				variable: false
// 			}, handler)
// 		},
// 		hide() {
// 			this.private = true;
// 			return this;
// 		},
// 		require() {
// 			this.required = true;
// 			return this;
// 		},
// 		dynamic() {
// 			this.variable = true;
// 			return this;
// 		}
// 	}
// );

// export const Value = Type(
// 	META_TYPES.value,
// 	construct.object,
// 	{
// 		constructor(value = null, priv = false) {
// 			Descriptor.prototype.constructor.call(this, {
// 				value,
// 				private: priv
// 			});
// 		}
// 	},
// 	{}, {},
// 	[Descriptor]
// );

// export const Variable = Type(
// 	META_TYPES.variable,
// 	construct.object,
// 	{
// 		constructor(value = null, priv = false, required = false) {
// 			this.super(Descriptor, {
// 				value,
// 				private: priv,
// 				required
// 			});
// 		}
// 	},
// 	{}, {},
// 	[Descriptor]
// );

// export const Accessor = Type(
// 	META_TYPES.accessor,
// 	construct.object,
// 	{
// 		constructor(get = DO_NOTHING, set = DO_NOTHING, required = false) {
// 			this.super(Descriptor, {
// 				get() {
// 					return get(this);
// 				},
// 				set(value) {
// 					return set(this, value);
// 				},
// 				required
// 			})
// 		}
// 	},
// 	{}, {},
// 	[Descriptor]
// );

// export const Template = Type(
// 	META_TYPES.template,
// 	construct.object,
// 	{
// 		constructor(id, creator = construct.object, descriptor = o(), static_descriptor = o(), ...parents) {
// 			let [prototype, properties, values] = Template.parse(descriptor);
// 			let [descriptors, raw] = Template.static_parse(static_descriptor);
// 			return Type(
// 				id, creator,
// 				prototype,
// 				properties,
// 				values,
// 				parents,
// 				construct.object(concat(descriptors, assemble.values(raw))),
// 			);
// 		}
// 	},
// 	{}, {}, [], {
// 		parse(descriptor) {
// 			return sort(descriptor, (key, value) =>
// 										type.function(value) ? 0 : 
// 											type.descriptor(value) ?
// 												1 : 2);
// 		},
// 		static_parse(descriptor) {
// 			return sort(descriptor, (key, value) =>
// 										type.descriptor(value) ? 0 : 1);
// 		},
// 	}
// );

// export const Param = Type(
// 	META_TYPES.param,
// 	construct.object,
// 	{
// 		constructor(T, def = null, req = false) {
// 			this.define({
// 				type: T,
// 				default: def,
// 				required: req
// 			})
// 		}
// 	}
// );

// export const RestParams = Type(
// 	META_TYPES.param
// );

// // Yeah... might move Modifier and Generator to inherit this one. Makes more sense maybe...
// export const Procedure = Type(
// 	META_TYPES.procedure,
// 	construct.object,
// 	{
// 		constructor(id, descriptor = o(), static_descriptor = o(), ...parents) {
// 			let [params, returns, template] = sort(descriptor, (key, value) =>
// 				value.satisfies && value.satisfies(Param) ? 0 : key === "returns" ? 1 : 2);
			
// 			let constructor = template.constructor ? function constructor(...args) {
// 				return values(params).every((param, index) => {
// 					return param.type.defines(args[index]);
// 				}) ?
// 					template.constructor.apply(this, args) : THROW(``)
// 			} : function constructor() {
// 				//
// 			};

// 			return this.super(Template, id, {
// 				...template,
// 				constructor,
// 			}, static_descriptor, ...parents);
// 		}
// 	}, {}, {},
// 	[Template]
// );


// export const Generator = Type(
// 	META_TYPES.generator,
// 	construct.function,
// 	// Do we continue?
// );

// export const Modifier = Type(
// 	META_TYPES.modify,
// 	construct.function,
// 	// Do we continue?
// );
